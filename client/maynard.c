/*
 * Copyright (c) 2013 Tiago Vignatti
 * Copyright (c) 2013-2014, 2019 Collabora Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"

#ifdef HAVE_CANTERBURY
#include <canterbury/canterbury-platform.h>
#endif

#include <gtk/gtk.h>
#include <gdk/gdkwayland.h>
#include <glib-unix.h>

#include "agl-shell-client-protocol.h"
#include "agl-shell-desktop-client-protocol.h"

#include "maynard-resources.h"

#include "app-icon.h"
#include "favorites.h"
#include "launcher.h"
#include "panel.h"

#define LAUNCHER_APP_ID "maynard-launcher"
#define INDICATORS_MENU_APP_ID "maynard-indicators-menu"

// Preferred sizes
#define PANEL_WIDTH 100
#define INDICATORS_MENU_WIDTH 400
#define INDICATORS_MENU_HEIGHT 200

#ifdef DEBUG_TRACE_ENABLED
#define DEBUG_TRACE(fmt, args...) \
    {   \
                fprintf(stderr, "TRACE [%s][%s][%4d]:"fmt"\n", __FILE__, __func__, __LINE__,  ##args); \
    }
#else
#define DEBUG_TRACE(fmt, args...)
#endif

extern char **environ; /* defined by libc */

struct element {
  GtkWidget *window;
  GdkPixbuf *pixbuf;
  struct wl_surface *surface;
  int height;
  int width;
  int ready;
};

struct desktop {
  struct wl_display *display;
  struct wl_registry *registry;
  struct wl_output *output;
  struct agl_shell *ashell;
  struct agl_shell_desktop *ashell_desktop;
  int ready;
  struct wl_seat *seat;
  struct wl_pointer *pointer;

  GdkDisplay *gdk_display;

  struct element *background;
  struct element *panel;
  struct element *indicators_menu;
  struct element *launcher;

  gboolean launcher_visible;
  gboolean indicators_menu_visible;
  gboolean pointer_out_of_panel;
};

static gboolean panel_window_enter_cb (GtkWidget *widget,
    GdkEvent *event, struct desktop *desktop);
static gboolean panel_window_leave_cb (GtkWidget *widget,
    GdkEvent *event, struct desktop *desktop);

static void indicators_menu_toggle (GtkWidget *widget,
    struct desktop *desktop);
static void launcher_toggle (GtkWidget *widget,
    struct desktop *desktop);

static void launcher_activate (struct desktop *desktop);
static void launcher_deactivate (struct desktop *desktop);
static void indicators_menu_activate (struct desktop *desktop);
static void indicators_menu_deactivate (struct desktop *desktop);
static void all_deactivate (struct desktop *desktop);

static gboolean
connect_enter_leave_signals (gpointer data)
{
  struct desktop *desktop = data;

  g_signal_connect (desktop->panel->window, "enter-notify-event",
      G_CALLBACK (panel_window_enter_cb), desktop);
  g_signal_connect (desktop->panel->window, "leave-notify-event",
      G_CALLBACK (panel_window_leave_cb), desktop);

  return G_SOURCE_REMOVE;
}

static void
wl_display_error (void *data,
                      struct wl_display *wl_display,
                      void *object_id,
                      uint32_t code,
                      const char *message)
{
  DEBUG_TRACE();
}

static void
wl_display_delete_id(void *data,
                          struct wl_display *wl_display,
                          uint32_t id)
{
  DEBUG_TRACE();
}

struct wl_display_listener display_listener = {
  wl_display_error,
  wl_display_delete_id
};

static void
agl_shell_desktop_application (void *data,
  struct agl_shell_desktop *agl_shell_desktop,
  const char *app_id)
{
  DEBUG_TRACE("APPLICATION app_id %s", app_id );
}

static void
agl_shell_desktop_state_app (void *data,
  struct agl_shell_desktop *agl_shell_desktop,
  const char *app_id,
  const char *app_data,
  uint32_t state,
  uint32_t role)
{
  DEBUG_TRACE("APPLICATION STATE app_id %s state %d role %d", app_id, state, role);
}

static const struct agl_shell_desktop_listener ashell_desktop_listener = {
   agl_shell_desktop_application,
   agl_shell_desktop_state_app
};

static void set_application_id (GtkWidget *widget, char * app_id)
{
  GdkWindow *gdk_window;
  gdk_window = gtk_widget_get_window (widget);
  gdk_wayland_window_set_application_id (gdk_window, app_id);
}

static void
all_deactivate (struct desktop *desktop)
{
  indicators_menu_deactivate (desktop);
  launcher_deactivate (desktop);
}

static void
launcher_create (struct desktop *desktop)
{
  struct element *launcher;
  GdkWindow *gdk_window;
  GdkScreen *screen = gdk_screen_get_default ();
  gint screen_width, screen_height;

  screen_width = gdk_screen_get_width (screen);
  screen_height = gdk_screen_get_height (screen);

  launcher = malloc (sizeof *launcher);
  memset (launcher, 0, sizeof *launcher);

  launcher->window = maynard_launcher_new (desktop->background->window);
  gdk_window = gtk_widget_get_window (launcher->window);
  launcher->surface = gdk_wayland_window_get_wl_surface (gdk_window);

  gtk_window_resize (GTK_WINDOW (launcher->window),
      screen_width - PANEL_WIDTH, screen_height);

  gtk_widget_show_all (launcher->window);

  desktop->launcher = launcher;
}

static void
launcher_activate (struct desktop *desktop)
{
  if (!desktop->launcher) {
    launcher_create (desktop);
    set_application_id (desktop->launcher->window, LAUNCHER_APP_ID);
  }
  else {
    agl_shell_desktop_activate_app (desktop->ashell_desktop, LAUNCHER_APP_ID, desktop, desktop->output);
  }
  desktop->launcher_visible = TRUE;
}

static void
launcher_deactivate (struct desktop *desktop)
{
  if (desktop->launcher && desktop->launcher_visible) {
    agl_shell_desktop_deactivate_app (desktop->ashell_desktop, LAUNCHER_APP_ID);
    desktop->launcher_visible = FALSE;
  }
}

static void
launcher_toggle (GtkWidget *widget,
    struct desktop *desktop)
{
  if (desktop->launcher_visible)
    {
      launcher_deactivate (desktop);
    }
  else
    {
      launcher_activate (desktop);
    }
}


static void
indicators_menu_create (struct desktop *desktop)
{
  struct element *indicators_menu;
  GdkWindow *gdk_window;
  GdkScreen *screen = gdk_screen_get_default ();
  gint screen_width, screen_height;

  screen_width = gdk_screen_get_width (screen);
  screen_height = gdk_screen_get_height (screen);

  indicators_menu = malloc (sizeof *indicators_menu);
  memset (indicators_menu, 0, sizeof *indicators_menu);

  indicators_menu->window = maynard_panel_get_indicators_menu (
      MAYNARD_PANEL (desktop->panel->window));
  gtk_window_set_title (GTK_WINDOW (indicators_menu->window), "maynard");
  gdk_window = gtk_widget_get_window (indicators_menu->window);
  indicators_menu->surface = gdk_wayland_window_get_wl_surface (gdk_window);

  gtk_window_resize (GTK_WINDOW (indicators_menu->window),
      INDICATORS_MENU_WIDTH, INDICATORS_MENU_HEIGHT);

  gtk_widget_show_all (indicators_menu->window);

  desktop->indicators_menu = indicators_menu;
}

static void
indicators_menu_activate (struct desktop *desktop)
{
  if (!desktop->indicators_menu) {
    GdkScreen *screen = gdk_screen_get_default ();
    gint screen_width, screen_height;

    screen_width = gdk_screen_get_width (screen);
    screen_height = gdk_screen_get_height (screen);

    agl_shell_desktop_set_app_property(desktop->ashell_desktop, INDICATORS_MENU_APP_ID,
                                       AGL_SHELL_DESKTOP_APP_ROLE_POPUP,
                                       PANEL_WIDTH, screen_height - INDICATORS_MENU_HEIGHT, 0, 0,
                                       INDICATORS_MENU_WIDTH, INDICATORS_MENU_HEIGHT,
                                       desktop->output);

    indicators_menu_create (desktop);
    set_application_id (desktop->indicators_menu->window, INDICATORS_MENU_APP_ID);
  }
  else {
    agl_shell_desktop_activate_app (desktop->ashell_desktop, INDICATORS_MENU_APP_ID, desktop, desktop->output);
  }
  desktop->indicators_menu_visible = TRUE;
}

static void
indicators_menu_deactivate (struct desktop *desktop)
{
  if (desktop->indicators_menu && desktop->indicators_menu_visible) {
    agl_shell_desktop_deactivate_app (desktop->ashell_desktop, INDICATORS_MENU_APP_ID);
    desktop->indicators_menu_visible = FALSE;
  }
}

static void
indicators_menu_toggle (GtkWidget *widget,
    struct desktop *desktop)
{
  if (desktop->indicators_menu_visible)
    {
      indicators_menu_deactivate (desktop);
    }
  else
    {
      indicators_menu_activate (desktop);
    }
}

static gboolean
panel_window_enter_cb (GtkWidget *widget,
    GdkEvent *event,
    struct desktop *desktop)
{
  desktop->pointer_out_of_panel = FALSE;
  return FALSE;
}

static gboolean
panel_window_leave_cb (GtkWidget *widget,
    GdkEvent *event,
    struct desktop *desktop)
{
  desktop->pointer_out_of_panel = TRUE;
  return FALSE;
}

static void
favorite_launched_cb (MaynardPanel *panel,
    struct desktop *desktop)
{
  if (desktop->launcher_visible)
    launcher_toggle (desktop->launcher->window, desktop);

  panel_window_leave_cb (NULL, NULL, desktop);
}

static void
panel_create (struct desktop *desktop)
{
  struct element *panel;
  GdkWindow *gdk_window;
  GdkScreen *screen = gdk_screen_get_default ();
  gint screen_width, screen_height;

  screen_width = gdk_screen_get_width (screen);
  screen_height = gdk_screen_get_height (screen);

  panel = malloc (sizeof *panel);
  memset (panel, 0, sizeof *panel);

  panel->window = maynard_panel_new ();

  g_signal_connect (panel->window, "app-menu-toggled",
      G_CALLBACK (launcher_toggle), desktop);
  g_signal_connect (panel->window, "indicators-menu-toggled",
      G_CALLBACK (indicators_menu_toggle), desktop);
  g_signal_connect (panel->window, "favorite-launched",
      G_CALLBACK (favorite_launched_cb), desktop);

  /* set it up as the panel */
  gdk_window = gtk_widget_get_window (panel->window);
  panel->surface = gdk_wayland_window_get_wl_surface (gdk_window);

  gtk_window_resize (GTK_WINDOW (panel->window),
    PANEL_WIDTH, screen_height);

  gtk_widget_show_all (panel->window);

  desktop->panel = panel;
}

/* Expose callback for the drawing area */
static gboolean
draw_cb (GtkWidget *widget,
    cairo_t *cr,
    gpointer data)
{
  struct desktop *desktop = data;

  gdk_cairo_set_source_pixbuf (cr, desktop->background->pixbuf, 0, 0);
  cairo_paint (cr);

  return TRUE;
}

/* Destroy handler for the window */
static void
destroy_cb (GObject *object,
    gpointer data)
{
  gtk_main_quit ();
}

static GdkPixbuf *
scale_background (GdkPixbuf *original_pixbuf)
{
  /* Scale original_pixbuf so it mostly fits on the screen.
   * If the aspect ratio is different than a bit on the right or on the
   * bottom could be cropped out. */
  GdkScreen *screen = gdk_screen_get_default ();
  gint screen_width, screen_height;
  gint original_width, original_height;
  gint final_width, final_height;
  gdouble ratio_horizontal, ratio_vertical, ratio;

  screen_width = gdk_screen_get_width (screen);
  screen_height = gdk_screen_get_height (screen);
  original_width = gdk_pixbuf_get_width (original_pixbuf);
  original_height = gdk_pixbuf_get_height (original_pixbuf);

  ratio_horizontal = (double) screen_width / original_width;
  ratio_vertical = (double) screen_height / original_height;
  ratio = MAX (ratio_horizontal, ratio_vertical);

  final_width = ceil (ratio * original_width);
  final_height = ceil (ratio * original_height);

  return gdk_pixbuf_scale_simple (original_pixbuf,
      final_width, final_height, GDK_INTERP_BILINEAR);
}

static void
background_create (struct desktop *desktop)
{
  GdkWindow *gdk_window;
  struct element *background;
  const gchar *filename;
  GdkPixbuf *unscaled_background;
  const gchar *xpm_data[] = {"1 1 1 1", "_ c SteelBlue", "_"};
  GdkScreen *screen = gdk_screen_get_default ();
  gint screen_width, screen_height;

  screen_width = gdk_screen_get_width (screen);
  screen_height = gdk_screen_get_height (screen);

  background = malloc (sizeof *background);
  memset (background, 0, sizeof *background);

  filename = g_getenv ("MAYNARD_BACKGROUND");
  if (filename && filename[0] != '\0')
    unscaled_background = gdk_pixbuf_new_from_file (filename, NULL);
  else
    unscaled_background = gdk_pixbuf_new_from_xpm_data (xpm_data);

  if (!unscaled_background)
    {
      g_message ("Could not load background (%s).",
          filename ? filename : "built-in");
      exit (EXIT_FAILURE);
    }

  background->pixbuf = scale_background (unscaled_background);
  g_object_unref (unscaled_background);

  background->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

  g_signal_connect (background->window, "destroy",
    G_CALLBACK (destroy_cb), NULL);
  g_signal_connect (background->window, "draw",
    G_CALLBACK (draw_cb), desktop);

  gtk_window_set_title (GTK_WINDOW (background->window), "maynard");
  gtk_window_set_decorated (GTK_WINDOW (background->window), FALSE);

  gtk_widget_realize (background->window);

  gtk_widget_set_size_request (background->window,
      screen_width, screen_height);

  gdk_window = gtk_widget_get_window (background->window);
  background->surface = gdk_wayland_window_get_wl_surface (gdk_window);

  gtk_widget_show_all (background->window);

  desktop->background = background;
}

static void
css_setup (struct desktop *desktop)
{
  GtkCssProvider *provider;
  GFile *file;
  GError *error = NULL;

  provider = gtk_css_provider_new ();

  file = g_file_new_for_uri ("resource:///org/maynard/style.css");

  if (!gtk_css_provider_load_from_file (provider, file, &error))
    {
      g_warning ("Failed to load CSS file: %s", error->message);
      g_clear_error (&error);
      g_object_unref (file);
      return;
    }

  gtk_style_context_add_provider_for_screen (gdk_screen_get_default (),
      GTK_STYLE_PROVIDER (provider), 600);

  g_object_unref (file);
}

static void
pointer_handle_enter (void *data,
    struct wl_pointer *pointer,
    uint32_t serial,
    struct wl_surface *surface,
    wl_fixed_t sx_w,
    wl_fixed_t sy_w)
{
}

static void
pointer_handle_leave (void *data,
    struct wl_pointer *pointer,
    uint32_t serial,
    struct wl_surface *surface)
{
}

static void
pointer_handle_motion (void *data,
    struct wl_pointer *pointer,
    uint32_t time,
    wl_fixed_t sx_w,
    wl_fixed_t sy_w)
{
}

static void
pointer_handle_button (void *data,
    struct wl_pointer *pointer,
    uint32_t serial,
    uint32_t time,
    uint32_t button,
    uint32_t state_w)
{
  struct desktop *desktop = data;

  if (state_w != WL_POINTER_BUTTON_STATE_RELEASED)
    return;

  if (!desktop->pointer_out_of_panel)
    return;

#if 0
  if (desktop->launcher_visible)
    launcher_deactivate (desktop);
  if (desktop->indicators_menu_visible)
    indicators_menu_deactivate (desktop);
#endif
}

static void
pointer_handle_axis (void *data,
    struct wl_pointer *pointer,
    uint32_t time,
    uint32_t axis,
    wl_fixed_t value)
{
}

static const struct wl_pointer_listener pointer_listener = {
  pointer_handle_enter,
  pointer_handle_leave,
  pointer_handle_motion,
  pointer_handle_button,
  pointer_handle_axis,
};

static void
seat_handle_capabilities (void *data,
    struct wl_seat *seat,
    enum wl_seat_capability caps)
{
  struct desktop *desktop = data;

  if ((caps & WL_SEAT_CAPABILITY_POINTER) && !desktop->pointer) {
    desktop->pointer = wl_seat_get_pointer(seat);
    wl_pointer_set_user_data (desktop->pointer, desktop);
    wl_pointer_add_listener(desktop->pointer, &pointer_listener,
          desktop);
  } else if (!(caps & WL_SEAT_CAPABILITY_POINTER) && desktop->pointer) {
    wl_pointer_destroy(desktop->pointer);
    desktop->pointer = NULL;
  }

  /* TODO: keyboard and touch */
}

static void
seat_handle_name (void *data,
    struct wl_seat *seat,
    const char *name)
{
}

static const struct wl_seat_listener seat_listener = {
  seat_handle_capabilities,
  seat_handle_name
};

static void
registry_handle_global (void *data,
    struct wl_registry *registry,
    uint32_t name,
    const char *interface,
    uint32_t version)
{
  struct desktop *d = data;

  if (!strcmp (interface, "wl_output"))
    {
      /* TODO: handle multiple outputs */
      if (d->output)
        return;
      d->output = wl_registry_bind (registry, name,
          &wl_output_interface, 1);
    }
  else if (!strcmp (interface, "wl_seat"))
    {
      /* TODO: handle multiple seats */
      if (d->seat)
        return;
      d->seat = wl_registry_bind (registry, name,
          &wl_seat_interface, 1);
      wl_seat_add_listener (d->seat, &seat_listener, d);
    }
  else if (!strcmp (interface, "agl_shell"))
    {
      d->ashell = wl_registry_bind (registry, name,
          &agl_shell_interface, 1);
    }
  else if (!strcmp (interface, "agl_shell_desktop"))
    {
      d->ashell_desktop = wl_registry_bind (registry, name,
          &agl_shell_desktop_interface, 1);
      agl_shell_desktop_add_listener (d->ashell_desktop, &ashell_desktop_listener, d);
      agl_shell_desktop_set_user_data (d->ashell_desktop, d);
    }
}

static void
registry_handle_global_remove (void *data,
    struct wl_registry *registry,
    uint32_t name)
{
}

static const struct wl_registry_listener registry_listener = {
  registry_handle_global,
  registry_handle_global_remove
};

gboolean time_handler(struct desktop *desktop) {

  if (!desktop->background->ready) {
    desktop->background->ready = 1;
    agl_shell_set_background(desktop->ashell, desktop->background->surface, desktop->output);
  }
  if (!desktop->panel->ready) {
    desktop->panel->ready = 1;
    agl_shell_set_panel(desktop->ashell, desktop->panel->surface, desktop->output, AGL_SHELL_EDGE_LEFT);
    connect_enter_leave_signals(desktop);
  }
  if (!desktop->ready) {
    desktop->ready = 1;
    agl_shell_ready(desktop->ashell);
  }
  return G_SOURCE_REMOVE;
}

static int
quit_handler ()
{
  gtk_main_quit ();

  return TRUE;
}

static void
setup_signals (void)
{
  signal (SIGPIPE, SIG_IGN);
  g_unix_signal_add (SIGINT, quit_handler, NULL);
  g_unix_signal_add (SIGTERM, quit_handler, NULL);
}

int
main (int argc,
    char *argv[])
{
  struct desktop *desktop;

#ifdef HAVE_CANTERBURY
  cby_init_environment ();
#endif

  gdk_set_allowed_backends ("wayland");

  gtk_init (&argc, &argv);

  g_resources_register (maynard_get_resource ());

  desktop = malloc (sizeof *desktop);
  desktop->output = NULL;
  desktop->ashell = NULL;
  desktop->ashell_desktop = NULL;
  desktop->ready = 0;
  desktop->seat = NULL;
  desktop->pointer = NULL;

  desktop->background = NULL;
  desktop->panel = NULL;
  desktop->indicators_menu = NULL;
  desktop->launcher = NULL;

  desktop->gdk_display = gdk_display_get_default ();
  desktop->display =
    gdk_wayland_display_get_wl_display (desktop->gdk_display);
  if (desktop->display == NULL)
    {
      fprintf (stderr, "failed to get display: %m\n");
      return -1;
    }

  wl_display_add_listener(desktop->display, &display_listener, desktop);

  desktop->registry = wl_display_get_registry (desktop->display);
  wl_registry_add_listener (desktop->registry,
      &registry_listener, desktop);

  /* Wait until we have been notified about the compositor,
   * shell, and shell helper objects */
  if (!desktop->output || !desktop->ashell ||
      !desktop->ashell_desktop) {
    wl_display_roundtrip (desktop->display);
  }
  if (!desktop->output || !desktop->ashell ||
      !desktop->ashell_desktop)
    {
      fprintf (stderr, "could not find output, shell or shell_desktop modules\n");
      return -1;
    }

  desktop->indicators_menu_visible = FALSE;
  desktop->launcher_visible = FALSE;
  desktop->pointer_out_of_panel = FALSE;

  css_setup (desktop);
  background_create (desktop);

  /* panel needs to be first so the launcher grid can
   * be added to its layer */
  panel_create (desktop);

  g_timeout_add(1000, (GSourceFunc) time_handler, (gpointer) desktop);

  setup_signals ();

  gtk_main ();

  /* TODO cleanup */
  return EXIT_SUCCESS;
}
